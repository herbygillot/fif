# Changelog
Dates are given in YYYY-MM-DD format.

## v0.3
### v0.3.3 (2021-mm-dd)
#### Features
- Added `--canonical-paths` flag for outputting canonical paths in output - for example,
  `mv /home/lynne/file.jpg /home/lynne/file.mp3` instead  of the default `mv file.jpg file.mp3`

### v0.3.2 (2021-06-14)
#### Bugfixes
- Fixed PowerShell output regression introduced in v0.2.13, whoops
#### Other
- Nicer version output: `fif -V` reports "fif v0.3.2" (instead of just "fif 0.3.2" without the "v"), and `fif --version`
  reports `fif v0.3.2 (XDG-Mime backend)`, or whatever backend you're using
- fif's trace output now includes its version, backend, operating system, and architecture
- Block compilation if both the `xdg-mime-backend` and `infer-backend`
  [features](https://gitlab.com/Lynnesbian/fif/-/wikis/Cargo-Features) are enabled

### v0.3.1 (2021-06-07)
#### Features
- Added JSON output support via `-o json`
- Added plaintext output support via `-o text`
#### Other
- `videos` is now an alias for `video`
- More extensive README documentation

### v0.3.0 (2021-04-28)
#### Features
- Added `-x`/`--exclude` flag for excluding file extensions (overrides `-e` or `-E` - `-E images -x jpg` scans all image
  files, except ".jpg" files)
- Added `-X`/`--exclude-set` flag for excluding sets of files, with the same syntax and sets as `-E`
- In addition to supplying included extensions as a comma separated list (like `-e jpg,png`), it is now possible to
  supply them through multiple uses of the `-e` flag (like `-e jpg -e png`). This also applies to `-x`
- `-e` and `-E` no longer conflict with each other, and can now be used together. For example, `-E images -e mp3`
  will scan all images *and* all MP3 files
- It is now possible to specify multiple extension sets at once: `-E images,system` will scan all images and archives
- fif's output now includes the directory it was run from
- Added `-q`/`--quiet` flag for reducing output verbosity
#### Bugfixes
- Resolved some discrepancies between `application/xml` and `text/xml`
#### Other
- Published my fork of ['mime_guess'] as ['new_mime_guess'], allowing it to be used properly with
  [crates.io](https://crates.io)
- The `videos` extension set has been renamed to `video`, in line with `audio`. `fif --help` has actually mistakenly
  referred to the set as `video` since v0.2.12! 0uo
- CI has been vastly improved
- Changed default verbosity to `info`

## v0.2
### v0.2.13 (2021-04-26)
#### Features
- Added `-v`/`--verbose` flag for setting verbosity without using `RUST_LOG`
- Added system extension set (`.dll`, `.so`, `.exe`...)
- Output is now sorted: Files that couldn't be read, then files with no known mimetype, then files with no known
  extensions, then files with the wrong extension
#### Bugfixes
- Fixed some bad formatting in PowerShell output
- Always quote file paths in output, even when not necessary - This makes output more portable and less likely to break
  in future, or if [`snailquote`] misses something
#### Other
- Added Apple iWork document formats to documents extension set
- Cleaned up and properly documented tests
- Renamed `Script` (in `formats.rs`) to `Shell`, in line with renaming in `parameters.rs`
- Added .rpa (Ren'Py archive) support to infer backend
- [`xdg-mime`] no longer uses git version
- Output `\r\n` on Windows
- Use a macro to generate `Writable` arrays, making the code a little cleaner and nicer to write

### v0.2.12 (2021-04-14)
#### Features
- Added Text extension set
- Better documentation for command line arguments
#### Bugfixes
- Fixed a very minor output bug relating to scanning symlinked directories
- Better detection for pre-OOXML Office files
#### Other
- Much better README.md
- Added more stuff to test.py
- PKGBUILD for Arch-based distros
- More test coverage
- Doubled BUF_SIZE

### v0.2.11 (2021-04-04)
#### Features
- fif can now traverse symlinks with the `-f`/`--follow-symlinks` flag
- Extensions are no longer mandatory - running fif without `-e` or `-E` will scan all files, regardless of extension
  (files without extensions are still skipped unless the -S flag is used)
#### Bugfixes
- Fixed compilation on big endian 32-bit architectures (see
  [here](https://github.com/bodil/smartstring/blob/v0.2.6/src/config.rs#L101-L103) for why that was a problem in the
  first place)
- Fixed broken tests for the [`infer`] backend
#### Other
- Better mime type detection:
  - Consider "some/x-thing" and "some/thing" to be identical
  - Use a patched version of mime_guess (which took a while to make 0u0;) with many more extension/type mappings

### v0.2.10 (2021-03-26)
- PowerShell support!

### v0.2.9 (2021-03-17)
- Replaced a bunch of `PathBuf`s with `Path`s, which should reduce memory usage
- Formatting improvements

### v0.2.8 (2021-03-03)
#### Features
- Added much more information - author, copyright, etc. - to `--help`/`-h` output
- Scan files without extensions with `-S` (by default, such files are ignored)
#### Bugfixes
- Using `-s` to skip hidden files no longer skips all files if the root directory itself is hidden
#### Other
- The `ScanError` enum now contains a `PathBuf` - Errors now return `ScanError` rather than `(ScanError, PathBuf)`
- Renamed modules in accordance with [Rust's API guidelines](https://rust-lang.github.io/api-guidelines/naming.html)

### v0.2.7 (2021-03-01)
- Default to `WARN`-level logging if `RUST_LOG` isn't set
- Added a drone CI config
- Added `test.py` for automated building and testing against Rust stable, beta, nightly, and the MSRV specified in
  `Cargo.toml`
- Added a test for argument parsing
- Documentation! And lots of it! 0u0

### v0.2.6 (2021-02-28)
- Added tests!
- Default to [`xdg-mime`] on all Unixy platforms, not just Linux - this also includes the various *BSDs (I've tested
  FreeBSD), macOS (haven't tested, but I have a very old MacBook running Leopard that has `file` preinstalled, so it
  *should* work fine), Redox OS (haven't tested), etc.

### v0.2.5 (2021-02-27)
- Use [`xdg-mime`] by default on Linux, [`infer`] elsewhere

### v0.2.4 (2021-02-22)
- Proper(ish) XML document support
- Display version in help output

### v0.2.3+hotfix (2021-02-22)
- A quick hack to fix broken/non-existent support for XML document files - `docx`, `odt`, etc.

### v0.2.3 (2021-02-22)
#### Features
- Automatically disable [`xdg-mime`] backend on Windows
- Exit codes
- Improved error handling
- Retrieve extension sets from [`mime_guess`] rather than hardcoding them
#### Bugfixes
- Improved SVG detection
#### Other
- Switched back from `printf` to `echo` in shell output
- More frequent and detailed comments
- Refactored `formats.rs`
- Exclude certain files and directories from the crate

### v0.2.2 (2021-02-20)
- Windows support

### v0.2.1 (2021-02-18)
#### Features
- Added extension sets -- you can now use, for example, `-E images` to check files with known image extensions
- Shell script output now uses `printf` instead of `echo`
- Added [`infer`] backend, configurable with [Cargo features](https://gitlab.com/Lynnesbian/fif/-/wikis/Cargo-Features)
#### Bugfixes
- Fixed broken singlethreaded support
#### Other
- Use a global backend instance instead of passing `&db` around constantly
- Use `rustfmt` 0u0

### v0.2.0 (2021-02-15)
#### Features
- Output a script rather than a list of misnamed files
- Parallel file scanning
- Added logging support
#### Bugfixes
- Handle filenames with invalid UTF-8
#### Other
- Added license
- Replaced [`structopt`] with [`clap`] 3 (beta)
- Specify 1.43.0 as minimum supported Rust version

## v0.1
### v0.1.0 (2021-02-04)
Initial commit!
- Only one backend - [`xdg-mime`]
- No output formats - just prints a list of files to be renamed
- Only supported flags are `-e` (specify extensions) and `-s` (scan hidden files)

<!-- links -->
[`xdg-mime`]: https://crates.io/crates/xdg-mime
[`structopt`]: https://crates.io/crates/structopt
[`clap`]: https://crates.io/crates/clap
[`infer`]: https://crates.io/crates/infer
[`mime_guess`]: https://crates.io/crates/mime_guess
[`new_mime_guess`]: https://crates.io/crates/new_mime_guess
[`snailquote`]: https://crates.io/crates/snailquote
