<div align="center">

# fif

![A screenshot demonstrating fif's ability to detect the correct file extensions for a few files.
](https://gitlab.com/Lynnesbian/fif/-/raw/master/doc/screenshot.png)

[![Version](https://img.shields.io/crates/v/fif.svg?logo=rust&style=flat-square)
](https://crates.io/crates/fif)
[![Minimum Supported Rust Version](https://img.shields.io/badge/msrv-1.43.0-orange?logo=rust&style=flat-square)
](https://crates.io/crates/fif)
[![License](https://img.shields.io/crates/l/fif.svg?style=flat-square)
](https://gitlab.com/Lynnesbian/fif/-/blob/master/LICENSE)
[![Build status](https://img.shields.io/gitlab/pipeline/Lynnesbian/fif/master?logo=gitlab&style=flat-square)
](https://gitlab.com/Lynnesbian/fif/-/pipelines/latest)
[![Unsafe forbidden](https://img.shields.io/badge/unsafe-forbidden-success.svg?style=flat-square)
](https://github.com/rust-secure-code/safety-dance/)

*A command-line tool for detecting and optionally correcting files with incorrect extensions.*
</div>

fif recursively scans the given directory and outputs a shell script to fix the name of any files with incorrect
extensions. By default, fif will scan all non-hidden files in the given directory, and will ignore symlinks.

As fif prints a shell script to stdout rather than acting on the files directly, you may wish to redirect its output to
a file, e.g. `fif ~/Documents > output.sh`. You can also pipe the output directly into your shell, e.g.
`fif ~/Documents | bash`, although this is not recommended - you should look over fif's output and verify for yourself
that it's not doing anything that will give you a headache before running it.

## Features
- :white_check_mark: Cross platform
- :white_check_mark: Multi-threaded
- :white_check_mark: Configurable

## Building
fif can be built, installed, and tested with [Cargo], as with most rust programs:
```bash
git clone https://gitlab.com/Lynnesbian/fif/
cd fif
# run tests (optional)
cargo test --locked
# build fif with its default feature set
cargo build --locked --release
```
The `--locked` flag ensures that Cargo uses the dependency versions specified in [the lock
file](https://gitlab.com/Lynnesbian/fif/-/blob/master/Cargo.lock), and the `--release` flag builds fif with release
optimisations enabled -- this takes longer, but produces a much faster binary.

### Installing
```bash
# install the fif crate (to ~/.cargo/bin or %USERPROFILE%\.cargo\bin by default)
cargo install --locked fif
```

To update, simply re-run the `install` command, or use a tool like [cargo-update
](https://github.com/nabijaczleweli/cargo-update), which can update crates installed via `cargo install`.

### Cargo Features
fif supports using [`infer`] or [`xdg-mime`] as its backend for looking up file types. By default, xdg-mime will be
used on [*nix systems](https://en.wikipedia.org/wiki/Unix-like) (Linux, macOS, *BSD, etc.), and infer on all other
systems.

`xdg-mime` should work on any *nix system with the [Shared MIME Info] library installed (consult your package
manager), although I've only tested it on Linux and FreeBSD. `infer` should work on any system.

You can override the default backend for your system at compile time like so:

```bash
# xdg-mime
cargo install fif --locked --features=xdg-mime-backend
# infer
cargo install fif --locked --features=infer-backend
```

It is also possible to get a more minimal build by installing without default features:
```bash
cargo install fif --locked --no-default-features
```
This will disable some non-essential but nice to have features, like multi-threading support.

For more info on fif's compile-time features, see [the wiki](https://gitlab.com/Lynnesbian/fif/-/wikis/Cargo-Features).

## Usage
See `fif --help` for more.

### The basics
The simplest way to use fif looks like this:

```bash
fif ~/Downloads
```

This command will scan all non-hidden files in your `~/Downloads` directory.

The `-e` and `-E` flags can be used to specify individual extensions and sets of extensions to scan, respectively:

```bash
# only scan files with the extensions .jpeg, .jpg, .zip, and .docx
fif -e jpeg,jpg,zip,docx ~/Documents
# only scan files with "image extensions" - .jpg, .png, .gif, .webp...
fif -E images ~/Pictures
# scan .zip files, videos, and audio
fif -e zip -E videos,audio ~/Downloads
```

Both `-e` and `-E` have equivalent `-x` and `-X` flags that exclude the given extensions rather than including them:
```bash
# scan everything except filenames ending in .zip
fif -x zip ~/Downloads
# scan all files with image extensions, but not .jpg and .jpeg files
fif -x jpg,jpeg -E images ~/Pictures
# scan everything except text and system files
fif -X text,system ~/.local/share
```

### Output
By default, fif will output a bash script (or PowerShell script on Windows) that can be used to fix all the files it
found with incorrect file extensions.

You might find it useful to output this script to a file (rather than to stdout):

```bash
fif ~/Documents > output.sh
```

You can also manually specify an output format to use:

```bash
fif -O powershell ~/Documents > output.ps1
```

### Logging
By default, fif will log any info, warnings, and errors encountered during execution. This can be changed with the `-v`
flag:
```bash
# also log debug info
fif -v ~/Downloads
# ...and trace info
fif -vv ~/Downloads
```

You can also reduce the level of logging with the `-q` flag:
```bash
# don't show info
fif -q ~/Downloads
# ...or warnings
fif -qq ~/Downloads
# ...or even errors!
fif -qqq ~/Downloads
```

The verbosity of the logging can also be modified by setting the environment variable `RUST_LOG` to `off`, `trace`,
`debug`, `info`, `warn`, or `error`. Values set by `RUST_LOG` override the `-v` and `-q` flags.

For example:

```bash
# show all levels except trace
RUST_LOG=debug fif ~/Downloads
# only show errors
RUST_LOG=error fif ~/Downloads
```

The five logging levels are used as follows:
| Level | Description | Example |
|-|-|-|
| error | Errors that cause fif to stop running | fif was unable to open the provided directory |
| warn | Warnings that don't cause fif to stop running | fif was unable to determine the mime type of a given file |
| info | Information pertaining to fif's status | The provided directory was scanned without issue, and no files are in need of renaming |
| debug | Debug information - usually not important to end users | The list of extensions fif will consider |
| trace | Trace info - usually not important to end users | "Found 15 items to check", "Scan successful", etc. |

For a more comprehensive explanation of all of fif's parameters and how to use them, run `fif --help` (or `fif -h` for
a more concise overview).

<!-- links -->
[Cargo]: https://doc.rust-lang.org/cargo/
[`xdg-mime`]: https://crates.io/crates/xdg-mime
[`infer`]: https://crates.io/crates/infer
[Shared MIME Info]: https://gitlab.freedesktop.org/xdg/shared-mime-info/