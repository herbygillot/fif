#[allow(unreachable_code, clippy::pedantic)]
fn main() -> Result<(), String> {
	#[cfg(all(feature = "infer-backend", feature = "xdg-mime-backend"))]
	// fail build if the user has set both the infer and xdg-mime backends
	return Err(String::from(
		"fif cannot be compiled with multiple backends set - please enable only one, or use the default.",
	));

	Ok(())
}
