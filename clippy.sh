#!/bin/bash
set -e
source $HOME/.cargo/env || true

_extra=""
_ver=""
if [ "$1" == "ci" ]; then
  # deny on warnings when running in CI
 _extra="-Dwarnings"
elif [ "$1" == "nightly" ]; then
	_ver="+nightly"
fi

# allow find to fail
find . -name '*.rs' -exec touch "{}" \; || true

_backends=( "xdg-mime-backend" "infer-backend" )

for backend in "${_backends[@]}"; do
  cargo $_ver clippy --tests --features="$backend" -- \
   -W clippy::nursery \
   -W clippy::perf \
   -W clippy::pedantic \
   -W clippy::complexity \
   -W clippy::cargo \
   -W clippy::float_cmp_const \
   -W clippy::lossy_float_literal \
   -W clippy::multiple_inherent_impl \
   -W clippy::string_to_string \
   -A clippy::unused_io_amount \
   -A clippy::redundant_closure_for_method_calls \
   -A clippy::shadow_unrelated \
   -A clippy::option_if_let_else \
   "$_extra"
done

# ALLOWS:
# unused_io_amount: there are two places where i want to read up to X bytes and i'm fine with getting less than that
# redundant_closure...: the alternative is often much more verbose
# shadow_unrelated: sometimes things that seem unrelated are actually related ;)
# option_if_let_else: the suggested code is usually harder to read than the original

