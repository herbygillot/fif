use cfg_if::cfg_if;

cfg_if! {
	if #[cfg(not(all(target_endian = "big", target_pointer_width = "32")))] {
		// most architectures
		pub use smartstring::alias::String;
	} else {
		// powerpc and other big endian 32-bit archs
		pub use std::string::String;
	}
}
