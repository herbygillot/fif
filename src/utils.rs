use cfg_if::cfg_if;
use once_cell::sync::OnceCell;

/// The current version of fif, as defined in Cargo.toml.
pub const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");

cfg_if! {
	if #[cfg(any(all(unix, feature = "infer-backend"), all(not(unix), not(feature = "xdg-mime-backend"))))] {
		/// The backend being used; either "Infer" or "XDG-Mime".
		pub const BACKEND: &str = "Infer";
	} else {
		/// The backend being used; either "Infer" or "XDG-Mime".
		pub const BACKEND: &str = "XDG-Mime";
	}
}

// the version and long_version given to clap need to be a &str, but we want to use format!, which returns a String.
// we can't just do something like `version = format!(...).as_str()`, because clap needs to know that the version will
// live for a given lifetime, which we need to satisfy by making our String static. of course, you can't use format!
// statically, so we need to use a OnceCell or similar to get around this.
static CLAP_VERSION: OnceCell<String> = OnceCell::new();
static CLAP_LONG_VERSION: OnceCell<String> = OnceCell::new();

/// Sets [`CLAP_VERSION`] to be the version defined in Cargo.toml, prefixed with a v (e.g. "v0.3.1"), then returns it as
/// a String.
pub fn clap_version() -> &'static str { CLAP_VERSION.get_or_init(|| format!("v{}", VERSION.unwrap_or("???"))) }

/// Sets [`CLAP_LONG_VERSION`] to be similar to [`CLAP_VERSION`], followed by the chosen backend in parentheses (e.g.
/// "v0.3.1 (XDG-Mime backend)"), then returns it as a String.
pub fn clap_long_version() -> &'static str {
	CLAP_LONG_VERSION.get_or_init(|| format!("v{} ({} backend)", VERSION.unwrap_or("???"), BACKEND))
}

/// Returns the name of the target operating system, like "Windows" or "macOS". Won't account for things like Wine or
/// Linuxulator.
#[allow(clippy::option_map_unit_fn)]
pub fn os_name() -> String {
	use std::env::consts::OS;
	match OS {
		"ios" => "iOS".into(),
		"macos" => "macOS".into(),
		"freebsd" => "FreeBSD".into(),
		"openbsd" => "OpenBSD".into(),
		"netbsd" => "NetBSD".into(),
		"vxworks" => "VxWorks".into(),
		_ => {
			// generic case: return consts::OS with the first letter in uppercase ("linux" -> "Linux")
			let mut os_upper = String::from(OS);
			os_upper.get_mut(0..1).map(|first| first.make_ascii_uppercase());
			os_upper
		}
	}
}
