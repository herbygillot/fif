#!/usr/bin/env python

import re
import subprocess
import sys

def test_archs():
	archs = ["aarch64", "powerpc"]
	upto = 1
	target = len(archs)

	for arch in archs:
		print(f"Testing {arch} ({upto} of {target})")
		subprocess.run(f"cross test --features=infer-backend --target {arch}-unknown-linux-gnu".split(" ")).check_returncode()
		upto += 1

def test_versions():
	match = re.search(
		r'rust-version ?= ?"([\d.]+)"',
		open("Cargo.toml", "r").read(-1)
	)

	if not match:
		print("Couldn't find rust-version")
		exit(1)

	versions = [match.group(1), "stable", "nightly"]
	backends = ["xdg-mime", "infer"]

	upto = 1
	target = len(versions) * len(backends) * 2

	for version in versions:
		for backend in backends:
			print(f"[{version}, {backend}] Tests ({upto} of {target})")
			subprocess.run(f"cargo +{version} test --features={backend}-backend".split(" ")).check_returncode()
			upto += 1

			print(f"[{version}, {backend}] Scanning imgs ({upto} of {target})")
			subprocess.run(f"cargo +{version} run --release --features={backend}-backend -- imgs".split(" ")).check_returncode()
			upto += 1

def main():
	done_something = False
	if "versions" in sys.argv:
		test_versions()
		done_something = True
	if "archs" in sys.argv:
		test_archs()
		done_something = True

	if not done_something:
		print("You must supply at least one of `versions` or `archs` as an argument! 0uo")
		sys.exit(2)

	print("Done! You might want to run cargo clean...")

if __name__ == "__main__":
	main()
